import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ISharedUserInfo } from '../../models/user';

@Injectable()
export class AuthService {
  baseUrl = '/auth';

  constructor(private http: Http) { }

  login(email, password): Observable<any> {
    return this
      .http
      .post(this.baseUrl, { email, password });
  }

  signup(userData: ISharedUserInfo): Observable<any> {
    return this.http.post(this.baseUrl, userData);
  }

}
