import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { List, Item } from '../../models';

@Injectable()
export class BoardsService {

  generateList(size = 3): List {
    return List.generateList({ length: size });
  }

  generateLists(length = 3): List[] {
    return Array.from({ length }, _ => this.generateList());
  }
}
