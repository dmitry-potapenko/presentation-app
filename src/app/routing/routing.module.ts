import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RootComponent } from '../components/root/root.component';
import { MainComponent } from '../components/main/main.component';
import { BoardsComponent } from '../components/boards/boards.component';
import { BoardsHomeComponent } from '../components/boards-home/boards-home.component';
import { BOARD_ROUTES } from './board-routing';
import { RouteReuseStrategy } from '@angular/router';
import { CustomReuseStrategy } from './custom-reuse-strategy';
import { BoardResolver } from '../components/board/resolve.service';
import { CUSTOM_ROUTES } from './custom-routing';
import { RandomResolver } from '../components/random-guard/resolver';

const BOARDS_ROUTES: Routes = [
  {
    path: 'boards',
    component: BoardsComponent,
    children: [
      {
        path: '',
        component: BoardsHomeComponent
      },
      BOARD_ROUTES
    ]
  }
];

const ROUTES: Routes = [
  {
    path: '', component: MainComponent, data: { num: 123 }
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(ROUTES),
    RouterModule.forRoot(BOARDS_ROUTES),
    RouterModule.forRoot(CUSTOM_ROUTES)
    // RouterModule.forChild(CUSTOM_ROUTES) // for child module(lazy?) when we’re defining routes inside a child module
  ],
  exports: [RouterModule],
  providers: [{ provide: RouteReuseStrategy, useClass: CustomReuseStrategy }, BoardResolver, RandomResolver]
})
export class RoutingModule { }
