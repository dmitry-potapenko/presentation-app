import { Route } from '@angular/router';
import { BoardComponent } from '../components/board/board.component';
import { CustomReuseStrategy } from './custom-reuse-strategy';
import { BoardResolver } from '../components/board/resolve.service';

const BOARD_ROUTES: Route = {
  path: 'b/:id',
  component: BoardComponent,
  resolve: {
    lists: BoardResolver
  },
  data: {
    shouldReuse: true
  }
};

export { BOARD_ROUTES };