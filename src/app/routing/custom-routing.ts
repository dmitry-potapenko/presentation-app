import { Routes } from '@angular/router';
import { DirectiveComponent } from '../components/directive/directive.component';
import { RandomGuardComponent } from '../components/random-guard/random-guard.component';
import { RandomGuard } from '../components/random-guard/guard';
import { TrashComponent } from '../components/trash/trash.component';
import { RandomResolver } from '../components/random-guard/resolver';
import { Observable } from 'rxjs';

export const CUSTOM_ROUTES: Routes = [{
    path: '',
    children: [{
        path: 'directive', component: DirectiveComponent
    },
    {
        path: 'random-guard', 
        component: RandomGuardComponent,
        canActivate: [RandomGuard],
        resolve: {
            randomId: RandomResolver
        },
        data: {
            overDataField: Observable.of('observe for data')
        }
    }, {
        path: 'trash',
        component: TrashComponent
    }]
}];