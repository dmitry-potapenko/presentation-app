import { Directive } from '@angular/core';
import { HostListener, ElementRef, Renderer } from '@angular/core';

@Directive({
  selector: '[outside-click-painter]'
  // host: {
  //   '(document:click)': 'outsideClick($event.target)',
  // }
})
export class OutsideClickDirective {
  private _el: any;

  @HostListener('document:click', ['$event.target']) outsideClick(target) {
    if (!this._el.contains(target)) {
      this.renderer.setElementStyle(this._el, 'backgroundColor', this.generateColor());
    }
  }

  constructor(private elementRef: ElementRef, private renderer: Renderer) { }

  ngOnInit() {
    this._el = this.elementRef.nativeElement;
  }

  generateColor() {
    let colors = Array.from({ length: 3 }, _ => Math.floor((Math.random() * 255)).toString(16)).join('');
    return `#${colors}`;
  }
}
