import { Directive, ViewContainerRef, TemplateRef, Input, Renderer } from '@angular/core';

@Directive({
  selector: '[task]'
})
export class TaskDirective {
  listener: any;
  @Input('task') id;
  constructor(
    private containerRef: ViewContainerRef,
    private templateRef: TemplateRef<any>,
    private renderer: Renderer) { }
  ngOnInit() {
    // нужно создать view из template
    this.containerRef.createEmbeddedView(this.templateRef);
  }
  ngAfterViewInit() {
    // цепляем listener на отрендеренный элемент
    var el = this.renderer.selectRootElement(`#${this.id}`);
    this.listener = this.renderer.listen(el, 'change', this.onChecked.bind(this));
  }
  onChecked() {
    console.log('checked');
    this.containerRef.clear();
  }
  ngOnDestroy() {
    if (this.listener) {
      this.listener();
    }
  }
}