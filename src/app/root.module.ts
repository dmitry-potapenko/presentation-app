import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule }  from '@angular/http';

import { RootComponent } from './components/root/root.component';
import { MainComponent } from './components/main/main.component';
import { BoardsComponent } from './components/boards/boards.component';
import { BoardComponent } from './components/board/board.component';
import { HeaderComponent } from './components/header/header.component';
import { RoutingModule } from './routing/routing.module';
import { BoardsHomeComponent } from './components/boards-home/boards-home.component';
import { ListComponent } from './components/list/list.component';
import { ItemComponent } from './components/item/item.component';
import { TaskDirective } from './directives/kill-on-click/kill-on-click.directive';
import { BoardsService } from './services/boards/boards.service';
import { MaterialModule } from '@angular/material';
import { DirectiveComponent } from './components/directive/directive.component';
import { RandomGuardComponent } from './components/random-guard/random-guard.component';
import { RandomGuard } from './components/random-guard/guard';
import { TrashComponent } from './components/trash/trash.component';
import { OutsideClickDirective } from './directives/outside-click/outside-click.directive';

@NgModule({
  declarations: [
    RootComponent,
    MainComponent,
    BoardsComponent,
    BoardComponent,
    HeaderComponent,
    BoardsHomeComponent,
    ListComponent,
    ItemComponent,
    TaskDirective,
    DirectiveComponent,
    RandomGuardComponent,
    TrashComponent,
    OutsideClickDirective
  ],
  imports: [
    HttpModule,
    BrowserModule,
    RoutingModule,
    MaterialModule
  ],
  providers: [BoardsService, RandomGuard, Title],
  bootstrap: [RootComponent]
})
export class RootModule { }