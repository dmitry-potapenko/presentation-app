import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-boards-home',
  templateUrl: './boards-home.component.html',
  styleUrls: ['./boards-home.component.css']
})
export class BoardsHomeComponent implements OnInit {

  boards: Array<number>;

  constructor() {
    this.boards = [1, 2, 3];
  }

  ngOnInit() { }

}
