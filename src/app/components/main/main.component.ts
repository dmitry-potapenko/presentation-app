import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor(private http: Http) { }

  ngOnInit() {
    // Observable
    //   .interval(2000)
    //   .switch(() => this.http.get('http://vk.com'))
    //   .onErrorResumeNext(Observable.of(0))
    //   .subscribe(
    //   data => console.log(data))

    // // Observable
    // //   .onErrorResumeNext(
    // //   Observable
    // //     .interval(5000)
    // //     .map(() => this.http.get('http://vk.com'))
    // //     // .map(() => Observable.create())
    // //     .catch(err => Observable.empty())
    // //   )
    // //   .subscribe(
    // //   function (x) { console.log(x); },
    // //   function (x) { console.log(x); },
    // //   function () { console.log('DONE'); }
    // //   )

    // Observable.interval(1e4)
    //   .switchMap(() => Observable.of(123213312))
    //   .retryWhen(err => err.retry())
    //   .catch(err => Observable.empty())
    //   .subscribe(data => console.log(data));

  }



}
