import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-trash',
  templateUrl: './trash.component.html',
  styleUrls: ['./trash.component.css']
})
export class TrashComponent implements OnInit, OnDestroy {
  timer: number;

  constructor(public router: Router) { }

  redirect() {
    this.router.navigate(['/']);
  }

  ngOnInit() {
    this.timer = setTimeout(this.redirect.bind(this), 3e3);
  }

  ngOnDestroy() {
    if (this.timer) {
      clearTimeout(this.timer);
    }
  }

}
