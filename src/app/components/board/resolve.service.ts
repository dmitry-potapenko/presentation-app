import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BoardsService } from '../../services/boards/boards.service';
import { List } from '../../models';

@Injectable()
export class BoardResolver implements Resolve<List[]> {
  constructor(private boardService: BoardsService) {}
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    console.log('#id from board resolver', route.params.id);
    return this.boardService.generateLists();
  }
}
