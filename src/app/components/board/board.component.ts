import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { List } from '../../models';
import { Observable, Subscription } from 'rxjs';
import { ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class BoardComponent implements OnInit, OnDestroy {
  lists: Array<List>;
  boards: Array<number> = [1, 2, 3];
  paramsSubscription: Subscription;
  dataSub: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.paramsSubscription = this.getParams().subscribe(params => console.log(params));
    this.dataSub = this.activatedRoute.data.subscribe(data => {
      this.lists = data.lists;
    });
    console.log('#ngOnInit');
  }
  ngOnDestroy() {
    if (this.paramsSubscription && !this.paramsSubscription.closed) {
      this.paramsSubscription.unsubscribe();
    }
    console.log('#ngOnDestroy', this.paramsSubscription);
  }
  ngOnChanges(obj) {
    console.log('#ngOnChanges11');
  }
  ngDoCheck(obj) {
    console.log('#ngDoCheck');
  }
  ngAfterViewInit() {
    console.log('#ngAfterViewInit');
  }
  ngAfterViewChecked() {
    console.log('#ngAfterViewChecked');
  }
  ngAfterContentInit() {
    console.log('#ngAfterContentInit');
  }
  ngAfterContentChecked() {
    console.log('#ngContentChecked');
  }

  getParams(): Observable<any> {
    return this.activatedRoute.params;
  }
}
