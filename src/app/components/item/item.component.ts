import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Item } from '../../models/item';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent {
  @Output() dataChange = new EventEmitter;
  @Input() set data(item: Item) {
    this._item = item;
    this.dataChange.emit(item);
  }

  get data(): Item {
    return this._item;
  }

  _item: Item;
}
