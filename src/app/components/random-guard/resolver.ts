import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BoardsService } from '../../services/boards/boards.service';
import { List } from '../../models';

@Injectable()
export class RandomResolver implements Resolve<string> {
  constructor(private boardService: BoardsService) { }
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    console.log('#random resolver');
    return Math.floor(Math.random() * Date.now()).toString(16);
  }
}