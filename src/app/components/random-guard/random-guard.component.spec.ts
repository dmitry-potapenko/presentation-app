import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RandomGuardComponent } from './random-guard.component';

describe('RandomGuardComponent', () => {
  let component: RandomGuardComponent;
  let fixture: ComponentFixture<RandomGuardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RandomGuardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RandomGuardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
