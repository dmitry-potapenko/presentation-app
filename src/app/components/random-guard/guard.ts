import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class RandomGuard implements CanActivate {
    constructor(private router: Router) { }
    canActivate() {
        console.log('#random guard')
        return this.checkSomething();
    }

    checkSomething() {
        const newValue = Math.random() > 0.5;
        if (newValue) {
            return true;
        }
        this.router.navigate(['/trash']);
        return false;
    }
}
