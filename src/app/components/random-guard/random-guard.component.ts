import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-random-guard',
  templateUrl: './random-guard.component.html',
  styleUrls: ['./random-guard.component.css']
})
export class RandomGuardComponent implements OnInit {
  testArray = Observable.of([1, 2, 3]);
  value: number;
  value2: number;
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      console.log(data);
    });
    this.value2 = 123;
    setInterval(_ => {
      console.log('timeout');
      this.value = Math.random();
    }, 1e3);
  }

  testFunc() {
    console.log('Shows behaviour of Change Detector');
    return this.value;
  }

}
