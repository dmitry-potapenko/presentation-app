import { Component, OnInit, Input } from '@angular/core';
import { Item, List, IList } from '../../models';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  items: Array<Item>;
  id: string;
  name: string;
  @Input() list;
  @Input() interpolationValue;

  ngOnInit() {
    console.log('Interpolation value', this.interpolationValue);
  }
  ngAfterContentInit() {
    console.log('#List ngAfterContentInit');
  }
  ngAfterContentChecked() {
    console.log('#List ngContentChecked');
  }
}
