import { Item } from './item';

export interface IList {
  id: string;
  name: string;
  items: Array<Item>;
}

export class List implements IList {
  private _id: string;
  private _name: string;
  items: Array<Item>;
  constructor(name: string, id: string, items: Array<Item>) {
    this.name = name;
    this.id = id;
    this.items = items;
  }

  // GETTERS - SETTERS
  set name(value: string) {
    this._name = value;
  }
  get name(): string {
    return this._name;
  }
  set id(value: string) {
    this._id = value;
  }
  get id(): string {
    return this._id;
  }
  // Fabric
  static generateList({ name = 'default list', length = 5 }): List {
    let id = Item.generateId();
    return new List(`${name} - ${id}`, id, Array.from({ length }, _ => Item.generateItem()));
  }
}