export interface ISharedUserInfo {
  email: string;
  isAuthenticated: Boolean;
  firstName?: string;
  lastName?: string;
}

export class User implements ISharedUserInfo {
  firstName: string;
  lastName: string;
  email: string;
  isAuthenticated: Boolean = false;

  constructor(email: string, auth = false, firstName = '', lastName = '') {
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.isAuthenticated = auth;
  }
}