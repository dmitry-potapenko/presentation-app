export interface IItem {
  text: string;
  id: string;
}

export class Item implements IItem {
  private _text: string;
  private _id: string;

  constructor(text: string, id: string) {
    this.text = text;
    this.id = id;
  }
  // GETTERS - SETTERS
  set text(value: string) {
    this._text = value;
  }
  get text(): string {
    return this._text;
  }
  set id(value: string) {
    this._id = value;
  }
  get id(): string {
    return this._id;
  }
  // Fabric
  static generateItem(text = 'default name'): Item {
    let id = Item.generateId();
    return new Item(`${text} - ${id}`, id);
  }
  static generateId(): string {
    return Math.floor(Math.random() * Date.now()).toString(16);
  }
}